# Team Nijhuis - Systeemtestscript

__Let op! Bitbucket repository is openbaar!__

## Quicktest

Voer environment tests uit via enkel commando:

`sh <(curl -s https://bitbucket.org/teamnijhuis/testscript-omgeving/raw/master/quicktest/environment)`


Voer release tests uit via enkel commando:

`sh <(curl -s https://bitbucket.org/teamnijhuis/testscript-omgeving/raw/master/quicktest/release)`


De quicktests downloaden de inhoud van de branch master van het script van BitBucket, pakken deze uit in een tijdelijke map en starten het script. Na uitvoering van het script wordt de tijdelijke map weer verwijderd.


## Gebruik

Gebruik het commando `./check_env` om tests uit te voeren. De map `inc` moet dan ook aanwezig zijn in dezelfde map.

	Gebruik: check_env environment | release 

		environment  Controleer of serveromgeving geschikt is voor ExpressionEngine.
		release      Controleer of ExpressionEngine goed is geconfigureerd.

		Opties:
			MySQL informatie direct meegeven:
			-h      MySQL hostname        bijv. -h localhost
			-u      MySQL username        bijv. -u dev_testdatabase
			-p      MySQL password        bijv. -p wachtwoord123
			-d      MySQL database        bijv. -d dev_testdatabase

			Padinformatie direct meegeven:
			-path   Relatieve of          bijv. -path /home/username/public_html
			        absolute pad naar 
			        ExpressionEngine-map

			Overige opties:
			-o      Output schrijven      bijv. -o test_rapport.log
			        naar rapportage       Standaard ee_tests_datum.log

### Test serveromgeving

Deze test controleert of de hosting voldoet aan de minimale systeemeisen voor het draaien van ExpressionEngine. Deze test kan goed gebruikt worden als klanten hun eigen hostingsprovider willen houden.

Deze test controleert de volgende gegevens:

  * Unix based besturingssysteem
  * PHP versie minimaal 5.2.4
  * Memory limit groter of gelijk aan 32 Mb
  * JSON encode en decode functies bestaan
  * imagejpeg functie bestaat
  * GD library is ge&iuml;nstalleerd en in PHP geconfigureerd
  * imagegif functie bestaat
  * imagepng functie bestaat



```
	$ ./check_env environment

	Team Nijhuis - Validatiescript
	Versie 0.1 - juli 2013


	MySQL hostname: localhost
	MySQL username: root
	MySQL password: root
	MySQL database: tn

	OK			os
	OK			php_version
	OK			memory_limit
	FAIL		mysql_version
				MySQL versie 5.0.3 is minimaal vereist.

	OK			json_functions
	OK			imagejpeg_function
	OK			gd_function
	OK			gd_function_exec_function
	OK			imagegif_function
	OK			imagepng_function
```

_Bovenstaande voorbeeld vraagt zelf om de overige parameters._


	$ ./check_env environment -h localhost -u username -p password -d databasenaam

	Team Nijhuis - Validatiescript
	Versie 0.1 - juli 2013


	OK			os
	OK			php_version
	OK			memory_limit
	FAIL		mysql_version
				MySQL versie 5.0.3 is minimaal vereist.

	OK			json_functions
	OK			imagejpeg_function
	OK			gd_function
	OK			gd_function_exec_function
	OK			imagegif_function
	OK			imagepng_function

_Bij het bovenstaande voorbeeld worden parameters al direct als argumenten meegegeven._


### Test ExpressionEngine installatie en configuratie

Deze test kan gedraaid worden na livegang om te controleren of ExpressionEngine goed is ingesteld.

De volgende gegevens worden gecontroleerd:

  * Mappen bestaan:
    * assets
    * beheer
    * beheer/codeigniter
    * beheer/expressionengine
    * beheer/expressionengine/cache
    * beheer/expressionengine/config
    * beheer/expressionengine/third_party
    * beheer/expressionengine/templates
    * beheer/logs
    * images
    * images/uploads
    * themes
    * Uploadmappen uit database

  * Bestanden bestaan:
    * index.php
    * admin.php
    * favicon.ico
    * robots.txt
    * beheer/expressionengine/config/config.php
    * beheer/expressionengine/config/database.php

  * Bestanden en mappen die niet bestaan:
    * phpdocs
    * phpdoc
    * phpdoc.xml
    * phpdocs.xml
    * phpinfo.php
    * info.php
    * README.md
    * beheer/installer

  * Juiste schrijfrechten:
    * beheer/expressionengine/config/config.php (666)
    * beheer/expressionengine/config/database.php (666)
    * beheer/expressionengine/cache (777)
    * beheer/logs (777)
    * images/uploads en submappen (777)

  * Database:
    * Verbindingsgegevens
    * Validatie tabellen (minimaal 78 tabellen aanwezig)
  
  * Accounts:
    * admin account ExpressionEngine aanwezig
    * teamnijhuis account ExpressionEngine aanwezig

  * Scriptvalidaties:
    * Debugmode config.php uit
    * Debugmode index.php uit
   





