<?php
class ReleaseCheck extends Check {

	/**
	 * Booleans die bijhouden of er een fout is opgetreden.
	 */
	private $folder_error_exists = FALSE;
	private $file_error_exists = FALSE;
	private $folder_nonexists_error_exists = FALSE;
	private $permissions_error_exists = FALSE;
	private $accounts_error_exists = FALSE;
	private $debugmode_error_exists = FALSE;


	public function __construct($out) {
		parent::__construct($out);

		// Array met checks, verwachting en meldingen

		$this->_checks = array(
				array('check_parameter_path',	''),
				array('check_database',			''),
				array('check_folders',			''),
				array('check_files',			''),
				array('check_folders_nonexists',''),
				array('check_permissions',		''),
				array('check_cms_accounts',		''),
				array('check_debug_mode',		'')
			);
	}

		

	/* ==== TESTS ==== */

	/**
	 * Test of $PATH parameter een geldige map is
	 */
	protected function check_parameter_path() {
		global $PATH;

		return (is_dir($PATH));
	}

	/**
	 * Test of databaseverbinding mogelijk is, database bestaat en er minimaal 78 tabellen zijn.
	 * 
	 * Kale installatie van ExpressionEngine genereert 78 tabellen.
	 */
	protected function check_database() {
		global $DB_HOST, $DB_USER, $DB_PWD, $DB_NAME;

		$con = @mysql_connect($DB_HOST, $DB_USER, $DB_PWD);

		if($con === FALSE) throw new Exception('Niet mogelijk om een verbinding te maken met de database.');
		if(mysql_select_db($DB_NAME, $con) === FALSE) throw new Exception('Niet mogelijk om de database te selecteren.');

		$tables_result = mysql_query('SHOW TABLES', $con);
		if(!(mysql_num_rows($tables_result) >= 78)) throw new Exception('Database mogelijk niet valide, er zijn minder dat 78 tabellen aanwezig.');

		return TRUE;
	}

	/**
	 * Controleer of alle benodigde statische mappen bestaan. 
	 */
	protected function check_folders() {
		global $PATH;

		if(!is_dir($PATH.'assets')) $this->print_folder_error('Map assets bestaat niet.');
		if(!is_dir($PATH.'beheer')) $this->print_folder_error('Map beheer map bestaat niet.');
		if(!is_dir($PATH.'beheer/codeigniter')) $this->print_folder_error('Map beheer/codeigniter map bestaat niet.');
		if(!is_dir($PATH.'beheer/expressionengine')) $this->print_folder_error('Map beheer/expressionengine map bestaat niet.');
		if(!is_dir($PATH.'beheer/expressionengine/cache')) $this->print_folder_error('Map beheer/expressionengine/cache map bestaat niet.');
		if(!is_dir($PATH.'beheer/expressionengine/config')) $this->print_folder_error('Map beheer/expressionengine/config map bestaat niet.');
		if(!is_dir($PATH.'beheer/expressionengine/third_party')) $this->print_folder_error('Map beheer/expressionengine/third_party map bestaat niet.');
		if(!is_dir($PATH.'beheer/expressionengine/templates')) $this->print_folder_error('Map beheer/expressionengine/templates map bestaat niet.');
		if(!is_dir($PATH.'beheer/logs')) $this->print_folder_error('Map beheer/logs map bestaat niet.');
		if(!is_dir($PATH.'images')) $this->print_folder_error('Map images map bestaat niet.');
		if(!is_dir($PATH.'images/uploads')) $this->print_folder_error('Map images/uploads map bestaat niet.');
		if(!is_dir($PATH.'themes')) $this->print_folder_error('Map themes map bestaat niet.');

		return !$this->folder_error_exists;
	}

	/**
	 * Controleer of enkele benodigde bestanden bestaan. 
	 */
	protected function check_files() {
		global $PATH;

		if(!is_file($PATH.'index.php')) $this->print_file_error('Bestand index.php bestaat niet.');
		if(!is_file($PATH.'admin.php')) $this->print_file_error('Bestand admin.php bestaat niet.');
		if(!is_file($PATH.'favicon.ico')) $this->print_file_error('Bestand favicon.ico bestaat niet.');
		if(!is_file($PATH.'robots.txt')) $this->print_file_error('Bestand robots.txt bestaat niet.');
		if(!is_file($PATH.'beheer/expressionengine/config/config.php')) $this->print_file_error('Bestand beheer/expressionengine/config/config.php bestaat niet.');
		if(!is_file($PATH.'beheer/expressionengine/config/database.php')) $this->print_file_error('Bestand beheer/expressionengine/config/database.php bestaat niet.');

		return !$this->file_error_exists;	
	}

	/**
	 * Controleer of bestanden en mappen met ontwikkelinformatie verwijderd zijn. 
	 */
	protected function check_folders_nonexists() {
		global $PATH;

		if(is_dir($PATH.'phpdocs')) $this->print_folder_nonexists_error('Map phpdocs bestaat nog.');
		if(is_dir($PATH.'phpdoc')) $this->print_folder_nonexists_error('Map phpdoc bestaat nog.');
		if(is_file($PATH.'phpdoc.xml')) $this->print_folder_nonexists_error('Bestand phpdoc.xml bestaat nog.');
		if(is_file($PATH.'phpdocs.xml')) $this->print_folder_nonexists_error('Bestand phpdocs.xml bestaat nog.');
		if(is_file($PATH.'phpinfo.php')) $this->print_folder_nonexists_error('Bestand phpinfo.php bestaat nog.');
		if(is_file($PATH.'info.php')) $this->print_folder_nonexists_error('Bestand info.php bestaat nog.');
		if(is_file($PATH.'README.md')) $this->print_folder_nonexists_error('Bestand README.md bestaat nog.');
		if(is_file($PATH.'readme.md')) $this->print_folder_nonexists_error('Bestand readme.md bestaat nog.');
		if(is_file($PATH.'readme')) $this->print_folder_nonexists_error('Bestand readme bestaat nog.');
		if(is_file($PATH.'readme.txt')) $this->print_folder_nonexists_error('Bestand readme.txt bestaat nog.');
		if(is_dir($PATH.'beheer/installer')) $this->print_folder_nonexists_error('Map beheer/installer bestaat nog.');

		return !$this->folder_nonexists_error_exists;
	}

	/**
	 * Controleer of alle rechten voor ExpressionEngine goed zijn ingesteld.
	 */
	protected function check_permissions() {
		global $PATH;

		$perms = $this->to_octal_pemissions($PATH.'beheer/expressionengine/config/config.php');
		if($perms != '0666') $this->print_permissions_error('Rechten config.php zijn niet 0666 maar '.$perms);

		$perms = $this->to_octal_pemissions($PATH.'beheer/expressionengine/config/database.php');
		if($perms != '0666') $this->print_permissions_error('Rechten database.php zijn niet 0666 maar '.$perms);

		$perms = $this->to_octal_pemissions($PATH.'beheer/expressionengine/cache');
		if($perms != '0777') $this->print_permissions_error('Rechten cache zijn niet 0777 maar '.$perms);

		$perms = $this->to_octal_pemissions($PATH.'beheer/logs');
		if($perms != '0777') $this->print_permissions_error('Rechten logs zijn niet 0777 maar '.$perms);

		$perms = $this->to_octal_pemissions($PATH.'images/uploads');
		if($perms != '0777') $this->print_permissions_error('Rechten images/uploads zijn niet 0777 maar '.$perms);

		$this->check_permissions_uploadpaths();

		return !$this->permissions_error_exists;
	}

	/**
	 * Controleer of juiste rechten voor upload paths in de database goed zijn ingesteld
	 * 
	 * Wordt aangeroepen vanuit ReleaseCheck::check_permissions().
	 * 
	 * @see RelaseCheck::check_permissions
	 */
	private function check_permissions_uploadpaths() {
		global $PATH;

		$con = $this->db_connection();
		
		$result = mysql_query('SELECT * FROM exp_upload_prefs', $con);
		if($result !== FALSE) {
			while($row = mysql_fetch_object($result)) {
				$upload_path = $PATH.substr(str_replace('../', '', $row->url), 1, strlen($row->url));
				$perms = $this->to_octal_pemissions($upload_path);
				if($perms != '0777') $this->print_permissions_error('Rechten images/uploads'.$row->url.' zijn niet 0777 maar '.$perms);
			}
		} else {
			mysql_close($con);
			throw new Exception('Upload preferences kunnen niet opgehaald worden, bestaat exp_upload_prefs wel?');
		}
		
		mysql_close($con);

		return !$this->permissions_error_exists;
	}

	/**
	 * Controleer of admin en teamnijhuis account zijn aangemaakt en of er 3 accounts in totaal zijn (dus ook klantaccount).
	 */
	protected function check_cms_accounts() {
		global $PATH;
		
		$con = $this->db_connection();
		
		$result = mysql_query('SELECT * FROM exp_members WHERE username = "admin"', $con);
		if($result !== FALSE) {
			if(mysql_num_rows($result) != 1) {
				$this->print_account_error('Er is geen gebruiker met username "admin" aangemaakt.');
			}
		} else {
			mysql_close($con);
			throw new Exception('Accounts kunnen niet opgehaald worden.');
		}

		$result = mysql_query('SELECT * FROM exp_members WHERE username = "teamnijhuis"', $con);
		if($result !== FALSE) {
			if(mysql_num_rows($result) != 1) {
				$this->print_account_error('Er is geen gebruiker met username "teamnijhuis" aangemaakt.');
			}
		} else {
			mysql_close($con);
			throw new Exception('Accounts kunnen niet opgehaald worden.');
		}
		
		$result = mysql_query('SELECT * FROM exp_members', $con);
		if($result !== FALSE) {
			if(mysql_num_rows($result) < 3) {
				$this->print_account_error('Er is zijn minder dan 3 CMS-accounts aanwezig. Zijn alle accounts aangemaakt?');
			}
		} else {
			mysql_close($con);
			throw new Exception('Accounts kunnen niet opgehaald worden.');
		}

		mysql_close($con);

		return !$this->accounts_error_exists;
	}

	/**
	 * Controleer of debug mode in config.php en index.php uitgeschakeld zijn.
	 */
	protected function check_debug_mode() {
		global $PATH;

		$index_content = @file_get_contents($PATH.'index.php');
		$matches = array();
		$match = preg_match('/[^*][\s]\Q$debug = \E+([\d])/', $index_content, $matches);
		$debug_mode_index = @$matches[count($matches)-1];
		if($debug_mode_index != 0) $this->print_debugmode_error('Debugmode in index.php is nog ingeschakeld.');

		$config_content = @file_get_contents($PATH.'beheer/expressionengine/config/config.php');
		$matches = array();
		$match = preg_match_all('/\Q$config[\'debug\'] = "\E+([\d])+\Q";\E/', $config_content, $matches);
		foreach($matches[1] as $k => $m) {
			if($m != 0) {
				$message = '';
				switch($k) {
					case 0: $message = 'Debugmode in config.php voor lokaal is niet uitgeschakeld.'; break;
					case 1: $message = 'Debugmode in config.php voor dev server is niet uitgeschakeld.'; break;
					case 2: $message = 'Debugmode in config.php voor live server is niet uitgeschakeld.'; break;
				}
				$this->print_debugmode_error($message);
			}
		}

		return !$this->debugmode_error_exists;
	}



	/**
	 * Haal rechten van map of bestand op en format naar octale notatie
	 */
	private function to_octal_pemissions($input) {
		return substr(sprintf('%o', @fileperms($input)), -4);
	}

	/**
	 * Maak verbinding met de database
	 */
	private function db_connection() {
		global $PATH,$DB_HOST, $DB_USER, $DB_PWD, $DB_NAME;

		$con = @mysql_connect($DB_HOST, $DB_USER, $DB_PWD);
		if(!$con) {
			throw new Exception('Er kan geen verbinding gemaakt worden met de database.');
		}
		if(mysql_select_db($DB_NAME, $con)) {
			return $con;
		} else {
			mysql_close($con);
			throw new Exception('De opgegeven database kan niet gevonden worden.');
		}
	}


	/* === Private functies === */
	
	/**
	 * Print foutmelding voor ontbrekende map
	 * 
	 * @param string Melding
	 */
	private function print_folder_error($message) {
		if(!$this->folder_error_exists) $this->output->write("\r\n");
		$this->print_error($message);
		$this->folder_error_exists = TRUE;
	}

	/**
	 * Print foutmelding voor ontbrekend bestand
	 * 
	 * @param string Melding
	 */
	private function print_file_error($message) {
		if(!$this->file_error_exists) $this->output->write("\r\n");
		$this->print_error($message);
		$this->file_error_exists = TRUE;
	}

	/**
	 * Print foutmelding voor mappen die niet verwijderd zijn
	 * 
	 * @param string Melding
	 */
	private function print_folder_nonexists_error($message) {
		if(!$this->folder_nonexists_error_exists) $this->output->write("\r\n");
		$this->print_error($message);
		$this->folder_nonexists_error_exists = TRUE;
	}

	/**
	 * Print foutmelding voor fout bij schrijfrechten
	 * 
	 * @param string Melding
	 */
	private function print_permissions_error($message) {
		if(!$this->permissions_error_exists) $this->output->write("\r\n");
		$this->print_error($message);
		$this->permissions_error_exists = TRUE;
	}

	/**
	 * Print foutmelding voor CMS-accounts
	 * 
	 * @param string Melding
	 */
	private function print_account_error($message) {
		if(!$this->accounts_error_exists) $this->output->write("\r\n");
		$this->print_error($message);
		$this->accounts_error_exists = TRUE;
	}

	/**
	 * Print foutmelding voor debug mode
	 * 
	 * @param string Melding
	 */
	private function print_debugmode_error($message) {
		if(!$this->debugmode_error_exists) $this->output->write("\r\n");
		$this->print_error($message);
		$this->debugmode_error_exists = TRUE;
	}

	/**
	 * Print foutmelding naar output buffer
	 */
	private function print_error($message) {
		$this->output->write("\t\t\t\t\033[0;31m".$message."\033[0m\r\n");
	}
}
?>