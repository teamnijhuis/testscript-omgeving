<?php
class Output {
	
	private $output_rapport = FALSE;
	private $output_rapport_file = '';

	private $output_buffer = '';

	public function __construct($output_rapport, $output_rapport_file) {
		$this->output_rapport = $output_rapport;
		$this->output_rapport_file = $output_rapport_file;
	}

	/**
	 * Schrijf output naar buffer of console
	 */
	public function write($message) {
		if($this->output_rapport) {
			$this->output_buffer .= $message;
		} else {
			echo $message;
		}
	}

	/**
	 * Schrijf output buffer naar rapport bestand
	 */
	public function write_buffer() {
		$this->output_buffer = preg_replace('/\033\[[0-9];[0-9]*m/', '', $this->output_buffer);
		$this->output_buffer = preg_replace('/\033\[0m/', '', $this->output_buffer);
		return file_put_contents($this->output_rapport_file, $this->output_buffer);
	}
}
?>