<?php
abstract class Check {

	/**
	 * @var Output Instantie van output class
	 */
	protected $output = NULL;

	/**
	 * @var array Array met functies die uitgevoerd moeten worden.
	 */
	protected $_checks = array();


	/**
	 * Constructor
	 * 
	 * @param Output Verwijzing naar output
	 */
	public function __construct(&$output) {
		$this->output = $output;
	}

	/**
	 * Voer checks uit
	 */
	public function run_check() {
		foreach($this->_checks as $check_key => $check_value) {
			try {
				$check_response = call_user_func(array($this, $check_value[0]));
				if($check_response == TRUE) {
					$this->output->write($this->color_green("\tOK\t\t\t{$check_value[0]}\r\n"));
				} else {
					$this->output->write($this->color_red("\tFAIL\t\t\t{$check_value[0]}\r\n"));
					if(count($check_value) > 1 && $check_value[1] != '') {
						$this->output->write("\t\t\t\t{$this->color_red($check_value[1])}\r\n");
					}
					$this->output->write("\r\n");
				}
			} catch(Exception $e) {
				$this->output->write($this->color_red("\tFAIL\t\t\t{$check_value[0]}\r\n"));
				$this->output->write("\t\t\t\t{$this->color_red($e->getMessage())}\r\n");
				$this->output->write("\r\n");
			}
		}
	}

	/**
	 * Kleur tekst rood in console
	 */
	private function color_red($text) {
		return "\033[0;31m".$text."\033[0m";
	}

	/**
	 * Kleur tekst groen in console
	 */
	private function color_green($text) {
		return "\033[0;32m".$text."\033[0m";
	}
}
?>