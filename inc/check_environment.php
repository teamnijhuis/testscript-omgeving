<?php
class EnvironmentCheck extends Check {

	/**
	 * @see Check
	 */
	protected $_checks = array();


	public function __construct(&$out) {
		parent::__construct($out);

		// Array met checks, verwachting en meldingen

		$this->_checks = array(
				array('os',							'ExpressionEngine draait beter op een unix bases systeem zoals Linux of OS X.'),
				array('php_version',				'PHP versie 5.2.4 is minimaal vereist.'),
				array('memory_limit',				'Memory limit moet minimaal 32M zijn.'),
				array('mysql_version',				'MySQL versie 5.0.3 is minimaal vereist.'),
				array('json_functions',				'JSON encode en/of decode functie is niet gevonden.'),
				array('imagejpeg_function',			'Functie imagejpeg bestaat niet.'),
				array('gd_function',				'GD is mogelijk niet geïnstalleerd.'),
				array('gd_function_exec_function',	'GD is niet geïnstalleerd en exec kan niet uitgevoerd worden.'),
				array('imagegif_function',			'Functie imagegif is niet gevonden.'),
				array('imagepng_function',			'Functie imagepng is niet gevonden.')
			);
	}



	/* ==== TESTS ==== */

	/**
	 * Check op unix bases systeem
	 */
	protected function os() {
		return strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN';
	}

	/**
	 * Check PHP versie
	 * 
	 * Versie moet groter of gelijk zijn aan 5.2.4
	 */
	protected function php_version() {
		return version_compare(phpversion(), '5.2.4', '>=');
	}

	/**
	 * Check minimale memory limit
	 */
	protected function memory_limit() {
		$memory_limit = @ini_get('memory_limit');

		sscanf($memory_limit, "%d%s", $limit, $unit);

		if ($limit >= 32) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * Check versie van MySQL
	 */
	protected function mysql_version() {
		global $DB_HOST, $DB_USER, $DB_PWD, $DB_NAME;
		$con = @mysql_connect($DB_HOST, $DB_USER, $DB_PWD);
		if(!$con) {
			throw new Exception('Onjuiste inloggegevens voor MySQL.');
		}
		if (version_compare(@mysql_get_server_info(), '5.0.3', '>=') !== TRUE) {
			return FALSE;
		} else {
			return TRUE;
		}
	}


	/**
	 * Check json functies
	 */
	protected function json_functions() {
		// Check for json_encode and decode
		if ( ! function_exists('json_encode'))
		{
			return FALSE;
		}
		if ( ! function_exists('json_decode'))
		{
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * Check functie imagejpeg
	 */
	protected function imagejpeg_function() {
		return (function_exists('imagejpeg'));
	}

	/**
	 * Check GD library
	 */
	protected function gd_function() {
		return (function_exists('gd_info'));	
	}

	/**
	 * Check GD functionaliteit
	 */
	protected function gd_function_exec_function() {
		return (function_exists('gd_info') OR function_exists('exec'));
	}

	/**
	 * Check functie imagegif
	 */
	protected function imagegif_function() {
		return (function_exists('imagegif'));
	}

	/**
	 * Check functie imagepng
	 */
	protected function imagepng_function() {
		return (function_exists('imagepng'));
	}

}
?>